﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Contract
{
    public interface IOperation
    {
        string Name { get; }
        double Calculate(double number1, double number2);
    }
}
