﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Contract
{
    public interface IDisplay
    {
        string Value { get; set; }

        void Clear();
    }
}
