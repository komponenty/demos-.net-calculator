﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Contract
{
    public interface ICalculations
    {
        IEnumerable<IOperation> AllowedOperations { get; }

        void PerformOperation(IOperation operation, double number);
    }
}
