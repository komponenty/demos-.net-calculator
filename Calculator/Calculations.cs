﻿using System;
using System.Collections.Generic;

using Calculator.Contract;

namespace Calculator.Device
{
    class Calculations : ICalculations
	{
        internal IALU alu { private get;  set; }
        internal IRegister accumulator { private get; set; }
        internal IDisplay display { private get; set; }

		public IEnumerable<IOperation> AllowedOperations 
		{
            get { return alu.AllowedOperations; } 
		}
		  
		public void PerformOperation(IOperation operation, double number)
		{
            double result = operation.Calculate(accumulator.Retrive(), number);
		    accumulator.Store(result);
            display.Value = result.ToString();
		}
	}
}
