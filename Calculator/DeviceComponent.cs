﻿using System;
using ComponentFramework;

using Calculator.Contract;

namespace Calculator.Device
{
    public class DeviceComponent : AbstractComponent
    {
        Calculations calculations;
        MemoryOperations memoryOperations;

        public DeviceComponent()
        {
            calculations = new Calculations();
            memoryOperations = new MemoryOperations();
            RegisterProvidedInterface<ICalculations>(calculations);
            RegisterProvidedInterface<IMemoryOperations>(memoryOperations);
            RegisterRequiredInterface<IALU>();
            RegisterRequiredInterface<IMemory>();
            RegisterRequiredInterface<IDisplay>();
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (type.Equals(typeof(IALU)))
            {
                calculations.alu = (impl as IALU);
            }
            if (type.Equals(typeof(IMemory)))
            {
                IRegister accumulator = (impl as IMemory).GetOrCreateRegister("accumulator");
                IRegister internalMemory = (impl as IMemory).GetOrCreateRegister("internalMemory");
                calculations.accumulator = accumulator;
                memoryOperations.accumulator = accumulator;
                memoryOperations.memory = internalMemory;
            }
            if (type.Equals(typeof(IDisplay)))
            {
                calculations.display = impl as IDisplay;
                memoryOperations.display = impl as IDisplay;
            }
        }
    }
}
