﻿using System;
using ComponentFramework;

using Calculator.Contract;

namespace Calculator.Display
{
    public class DisplayComponent : AbstractComponent, IDisplay
    {
        string value;

        public DisplayComponent()
        {
            Clear();
            RegisterProvidedInterface<IDisplay>(this);
        }

        public void Clear()
        {
            Value = "";
        }

        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
                Console.WriteLine("==> {0} <==", value);
            }
        }

        public override void InjectInterface(Type type, object impl)
        {
            // nothing expected
        }
    }
}
