﻿using System;
using System.Collections.Generic;
using ComponentFramework;

using Calculator.Contract;

namespace Calculator.ALU.Basic
{
    public class ALUComponent : AbstractComponent, IALU
    {
        List<IOperation> operations;

        public ALUComponent()
        {
            operations = new List<IOperation> 
		    {
		        new Add(), new Sub(), new Mul(), new Div()
		    };
            RegisterProvidedInterface<IALU>(this);
        }

        public IEnumerable<IOperation> AllowedOperations
        {
            get { return operations.AsReadOnly(); }
        }
        
        public override void InjectInterface(Type type, object impl)
        {
            // nothing expected
        }
    }
}
