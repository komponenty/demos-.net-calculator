﻿using System;
using System.Collections.Generic;

using Calculator.Contract;

namespace Calculator.Memory
{
    class Memory : IMemory
    {
        private IDictionary<string, IRegister> registers;

        public Memory()
        {
            registers = new Dictionary<string, IRegister>();
        }

        public IRegister CreateRegister(string name)
        {
            if (registers.ContainsKey(name))
                throw new ArgumentException(string.Format("Register with name {0} already existing", name));
            IRegister register = new Register();
            registers[name] = register;
            return register;
        }

        public IRegister GetRegister(string name)
        {
            return registers[name];
        }

        public IRegister GetOrCreateRegister(string name)
        {
            try
            {
                return GetRegister(name);
            }
            catch (Exception ex)
            {
                return CreateRegister(name);
            }
        }
    }
}
