﻿using System;
using ComponentFramework;

using Calculator.Contract;

namespace Calculator.Memory
{
    public class MemoryComponent : AbstractComponent
    {
        Memory memory;

        public MemoryComponent()
        {
            memory = new Memory();
            RegisterProvidedInterface<IMemory>(memory);
        }

        public override void InjectInterface(Type type, object impl)
        {
            // nothing expected
        }
    }
}
