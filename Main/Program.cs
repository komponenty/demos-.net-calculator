﻿using System;
using System.Collections.Generic;
using ComponentFramework;

using Calculator.ALU.Basic;
using Calculator.Display;
using Calculator.Memory;
using Calculator.Device;
using Calculator.Contract;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Container container = new Container();
            container.RegisterComponents(
                new ALUComponent(), 
                new DisplayComponent(),
                new MemoryComponent(),
                new DeviceComponent()
            );

            if (!container.DependenciesResolved)
            {
                Console.WriteLine("There are unresolved dependencies");
                foreach (var type in container.UnresolvedDependencies.Keys)
                {
                    Console.WriteLine("{0}: {1}", type, string.Join(", ", container.UnresolvedDependencies[type]));
                }
                Console.ReadKey();
                Environment.Exit(-1);
            }

            var calcs = container.GetInterface<ICalculations>();
            var operations = new List<IOperation>(calcs.AllowedOperations);
            ConsoleKeyInfo key;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Operacja");
                Console.WriteLine("========");
                for (var i = 0; i < operations.Count; ++i)
                    Console.WriteLine("{0}) {1}", i, operations[i].Name);
                Console.WriteLine("q) Quit\n");
                key = Console.ReadKey();
                if (key.KeyChar.Equals('q'))
                    break;
                var operation = operations[Convert.ToInt16(key.KeyChar.ToString())];
                Console.Write("\n\n> ");
                var number = Convert.ToDouble(Console.ReadLine());
                calcs.PerformOperation(operation, number);
            }
        }
    }
}
